from functools import reduce
from operator import add

from bearclaw.feature_extraction import _COSMIC_MUTATIONAL_SIGNATURES
import numpy as np
from numpy import argwhere, around, cumsum
import pandas as pd
import scipy as sp


SBS = pd.read_csv(
    _COSMIC_MUTATIONAL_SIGNATURES["single_base_substitutions"], sep="\t", index_col=0
).transpose()


def complement(sequence: str) -> str:
    """Complement nucleotide sequence."""
    complement = map(
        lambda c: {"A": "T", "C": "G", "T": "A", "G": "C"}[c], sequence.upper()
    )
    return reduce(add, complement)


def reverse_complement(sequence: str) -> str:
    """Reverse complement nucleotide sequence."""
    return complement(sequence)[::-1]


def pyrimidine_base_triplet(triplet: str) -> str:
    """The centre nucleotide is by convention a pyrimidine.

    See Bergstrom et al. BMC Genomics, 20:685 ('19).
    """
    if not triplet[1].upper() in ("T", "C"):
        return reverse_complement(triplet)
    return triplet


def pyrimidine_single_base_substitution(ref_triplet: str, alternate: str) -> str:
    """Apply pyrimidine convention to single base substitution."""
    if not ref_triplet[1].upper() in ("T", "C"):
        ref_triplet = reverse_complement(ref_triplet)
        alternate = complement(alternate)
    return (
        ref_triplet[0] + "[" + ref_triplet[1] + ">" + alternate + "]" + ref_triplet[2]
    )


def number_of_pyrimidines(sequence: str) -> int:
    """Count the number of pyrimidine DNA letters in sequence."""
    return sum([1 for c in sequence.upper() if c in ("T", "C")])


def pyrimidine_doublet_base_substitution(doublet_ref: str, doublet_alt: str):
    """Apply pyrimidine-most convention to doublet base substitution."""
    table = {
        "AA>CC": "TT>GG",
        "TT>GG": "TT>GG",
        "AA>CG": "TT>CG",
        "TT>CG": "TT>CG",
        "AA>CT": "TT>AG",
        "TT>AG": "TT>AG",
        "AA>GC": "TT>GC",
        "TT>GC": "TT>GC",
        "AA>GG": "TT>CC",
        "TT>CC": "TT>CC",
        "AA>GT": "TT>AC",
        "TT>AC": "TT>AC",
        "AA>TC": "TT>GA",
        "TT>GA": "TT>GA",
        "AA>TG": "TT>CA",
        "TT>CA": "TT>CA",
        "AA>TT": "TT>AA",
        "TT>AA": "TT>AA",
        "AC>CA": "AC>CA",
        "GT>TG": "AC>CA",
        "AC>CG": "AC>CG",
        "GT>CG": "AC>CG",
        "AC>CT": "AC>CT",
        "GT>AG": "AC>CT",
        "AC>GA": "AC>GA",
        "GT>TC": "AC>GA",
        "AC>GG": "AC>GG",
        "GT>CC": "AC>GG",
        "AC>GT": "AC>GT",
        "GT>AC": "AC>GT",
        "AC>TA": "AC>TA",
        "GT>TA": "AC>TA",
        "AC>TG": "AC>TG",
        "GT>CA": "AC>TG",
        "AC>TT": "AC>TT",
        "GT>AA": "AC>TT",
        "AG>CA": "CT>TG",
        "CT>TG": "CT>TG",
        "AG>CC": "CT>GG",
        "CT>GG": "CT>GG",
        "AG>CT": "CT>AG",
        "CT>AG": "CT>AG",
        "AG>GA": "CT>TC",
        "CT>TC": "CT>TC",
        "AG>GC": "CT>GC",
        "CT>GC": "CT>GC",
        "AG>GT": "CT>AC",
        "CT>AC": "CT>AC",
        "AG>TA": "CT>TA",
        "CT>TA": "CT>TA",
        "AG>TC": "CT>GA",
        "CT>GA": "CT>GA",
        "AG>TT": "CT>AA",
        "CT>AA": "CT>AA",
        "AT>CA": "AT>CA",
        "AT>TG": "AT>CA",
        "AT>CC": "AT>CC",
        "AT>GG": "AT>CC",
        "AT>CG": "AT>CG",
        "AT>CG": "AT>CG",
        "AT>GA": "AT>GA",
        "AT>TC": "AT>GA",
        "AT>GC": "AT>GC",
        "AT>GC": "AT>GC",
        "AT>TA": "AT>TA",
        "AT>TA": "AT>TA",
        "CA>AC": "TG>GT",
        "TG>GT": "TG>GT",
        "CA>AG": "TG>CT",
        "TG>CT": "TG>CT",
        "CA>AT": "TG>AT",
        "TG>AT": "TG>AT",
        "CA>GC": "TG>GC",
        "TG>GC": "TG>GC",
        "CA>GG": "TG>CC",
        "TG>CC": "TG>CC",
        "CA>GT": "TG>AC",
        "TG>AC": "TG>AC",
        "CA>TC": "TG>GA",
        "TG>GA": "TG>GA",
        "CA>TG": "TG>CA",
        "TG>CA": "TG>CA",
        "CA>TT": "TG>AA",
        "TG>AA": "TG>AA",
        "CC>AA": "CC>AA",
        "GG>TT": "CC>AA",
        "CC>AG": "CC>AG",
        "GG>CT": "CC>AG",
        "CC>AT": "CC>AT",
        "GG>AT": "CC>AT",
        "CC>GA": "CC>GA",
        "GG>TC": "CC>GA",
        "CC>GG": "CC>GG",
        "GG>CC": "CC>GG",
        "CC>GT": "CC>GT",
        "GG>AC": "CC>GT",
        "CC>TA": "CC>TA",
        "GG>TA": "CC>TA",
        "CC>TG": "CC>TG",
        "GG>CA": "CC>TG",
        "CC>TT": "CC>TT",
        "GG>AA": "CC>TT",
        "CG>AA": "CG>TT",
        "CG>TT": "CG>TT",
        "CG>AC": "CG>GT",
        "CG>GT": "CG>GT",
        "CG>AT": "CG>AT",
        "CG>AT": "CG>AT",
        "CG>GA": "CG>TC",
        "CG>TC": "CG>TC",
        "CG>GC": "CG>GC",
        "CG>GC": "CG>GC",
        "CG>TA": "CG>TA",
        "CG>TA": "CG>TA",
        "GA>AC": "TC>GT",
        "TC>GT": "TC>GT",
        "GA>AG": "TC>CT",
        "TC>CT": "TC>CT",
        "GA>AT": "TC>AT",
        "TC>AT": "TC>AT",
        "GA>CC": "TC>GG",
        "TC>GG": "TC>GG",
        "GA>CG": "TC>CG",
        "TC>CG": "TC>CG",
        "GA>CT": "TC>AG",
        "TC>AG": "TC>AG",
        "GA>TC": "TC>GA",
        "TC>GA": "TC>GA",
        "GA>TG": "TC>CA",
        "TC>CA": "TC>CA",
        "GA>TT": "TC>AA",
        "TC>AA": "TC>AA",
        "GC>AA": "GC>AA",
        "GC>TT": "GC>AA",
        "GC>AG": "GC>AG",
        "GC>CT": "GC>AG",
        "GC>AT": "GC>AT",
        "GC>AT": "GC>AT",
        "GC>CA": "GC>CA",
        "GC>TG": "GC>CA",
        "GC>CG": "GC>CG",
        "GC>TA": "GC>TA",
        "GC>TA": "GC>TA",
        "TA>AC": "TA>GT",
        "TA>GT": "TA>GT",
        "TA>AG": "TA>CT",
        "TA>CT": "TA>CT",
        "TA>AT": "TA>AT",
        "TA>AT": "TA>AT",
        "TA>CC": "TA>GG",
        "TA>GG": "TA>GG",
        "TA>CG": "TA>CG",
        "TA>GC": "TA>GC",
    }
    if (doublet_ref[0] == doublet_alt[0]) or (doublet_ref[1] == doublet_alt[1]):
        raise ValueError(f"Not a doublet base substitution {doublet_ref}>{doublet_alt}")

    original_dbs = doublet_ref + ">" + doublet_alt
    return table[original_dbs]

    # rev_compl_dbs = (
    #     reverse_complement(doublet_ref) + ">" + reverse_complement(doublet_alt)
    # )
    # if number_of_pyrimidines(original_dbs) > number_of_pyrimidines(rev_compl_dbs):
    #     return original_dbs
    # elif number_of_pyrimidines(original_dbs) < number_of_pyrimidines(rev_compl_dbs):
    #     return rev_compl_dbs
    # # Number of pyrimidines is the same.
    # if original_dbs != rev_compl_dbs:
    #     # Ties were broken as in Bergstrom et al. BMC Genomics 20:685,
    #     # (2019), Supplementary Material.
    #     ties = {
    #         "AA>CC": "TT>GG",
    #         "AA>CT": "TT>AG",
    #         "AA>GT": "TT>AC",
    #         "AA>TC": "TT>GA",
    #         "AA>TT": "TT>AA",
    #         "AC>CA": "AC>CA",
    #         "AC>CG": "AC>CG",
    #         "AC>GT": "AC>GT",
    #         "AC>TA": "AC>TA",
    #         "AC>TG": "AC>TG",
    #         "AG>CC": "CT>GG",
    #         "AG>CT": "CT>AG",
    #         "AG>TC": "CT>GA",
    #         "AG>TT": "CT>AA",
    #         "AT>CA": "AT>CA",
    #         "CA>AC": "TG>GT",
    #         "CA>AT": "TG>AT",
    #         "CA>GC": "TG>GC",
    #         "CA>GT": "TG>AC",
    #         "CA>TG": "TG>CA",
    #         "CC>AA": "CC>AA",
    #         "CC>AG": "CC>AG",
    #         "CC>GA": "CC>GA",
    #         "CC>GG": "CC>GG",
    #         "CG>AC": "CG>GT",
    #         "GA>CC": "TC>GG",
    #         "GA>CT": "TC>AG",
    #         "GA>TC": "TC>GA",
    #         "GA>TT": "TC>AA",
    #         "GC>CA": "GC>CA",
    #         "TA>AC": "TA>GT",
    #     }
    #     return ties[original_dbs]
    # # The same reverse complement sequence, so original is fine.
    # return original_dbs


def quantile_threshold(signature, quantile: float) -> float:
    """Threshold captures at least `quantile` of all substitutions.

    Args:
        quantile: After thresholding, the signature probabilities sum up to
            quantile or greater.
    """
    values = sorted(signature)
    cumulative_distribution = around(cumsum(values), decimals=8)

    i_quantile = argwhere(~(cumulative_distribution < (1 - quantile)))[0, 0]
    threshold = values[i_quantile]
    return threshold


def normalise_columns(data: pd.DataFrame) -> pd.DataFrame:
    """Normalise dataframe across axis=1."""
    normaliser = data.sum(axis=1).values[:, None]
    return np.divide(data, normaliser)


def entropic_centre_of_mass(*attributions: pd.DataFrame) -> pd.Series:
    """Weighted average of entropy of used signatures."""
    s_sbs = sp.stats.entropy(SBS, axis=1)
    s_coms = []
    for attr in attributions:
        p_attr = normalise_columns(attr)
        s = (p_attr * s_sbs.reshape(1, -1)).sum(min_count=1, axis=1)
        s_coms.append(s)
    return tuple(s_coms)


def kendall_tau_correlation(
    dataframe: pd.DataFrame,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Pair-wise kendall-tau correlation with p-values."""
    cols = dataframe.columns
    correlation = pd.DataFrame(0.0, index=cols, columns=cols)
    pvalues = pd.DataFrame(1.0, index=cols, columns=cols)
    for i, r in enumerate(cols):
        for c in cols[i + 1 :]:
            test_result = sp.stats.kendalltau(dataframe[r], dataframe[c])
            correlation.loc[c, r] = correlation.loc[r, c] = test_result.statistic
            pvalues.loc[c, r] = pvalues.loc[r, c] = round(test_result.pvalue, 4)
    return correlation, pvalues
