"""Function to extrapolate in silico panel data to larger panel sizes."""
from bearclaw.feature_extraction import (
    POSSIBLE_SBS_SEQUENCING_ARTEFACTS,
    CosmicNMF,
)
import numpy as np
import pandas as pd
from sklearn.utils import resample
from statkit.non_parametric import bootstrap_score
from statkit.types import Estimate


def reduce_n_fold(X, n_fold, stratify):
    """Group and sum every `n_fold` rows within stratum.

    Args:
        X: Data to coarse grain.
        stratify: Coarse graining within same strata.
        n_fold: Merge this many samples.
    """
    coarse_gps = []
    for _, gp in X.groupby(stratify):
        idx_gp = np.arange(len(gp)) // n_fold
        coarse_gps.append(gp.groupby(idx_gp).sum())
    return pd.concat(coarse_gps)


def pair_wise_coarse_grain(
    *dataframes: pd.DataFrame,
    n_fold: int,
    stratify=None,
    random_state: int = 42,
) -> tuple[pd.DataFrame, ...]:
    """Paired coarse graining of dataframes by `n_fold` duplicating the samples.

    Args:
        *dataframes: Data to coarse grain over.
        stratify: Resample subjects within the same stratum.

    """
    if stratify is None:
        # All samples belong to the same stratum.
        stratify = np.ones(dataframes[0].shape[0])

    # Duplicate the samples `n_fold` times by resampling.
    m_target = dataframes[0].shape[0] * n_fold
    stratum, *dfs_expanded = resample(
        stratify,
        *dataframes,
        replace=True,
        n_samples=m_target,
        stratify=stratify,
        random_state=random_state,
    )

    if len(dataframes) == 1:
        # Convert dfs_expanded from dataframe to tuple of dataframes.
        dfs_expanded = (dfs_expanded,)
    else:
        # Check consistency with other dataframe.
        assert all(dfs_expanded[0].index == dfs_expanded[1].index)

    # Groupby every `n_fold` rows with stratum, summing the values.
    df_coarse_grained = tuple(reduce_n_fold(df, n_fold, stratum) for df in dfs_expanded)
    return df_coarse_grained


def signature_fnr_from_spectrum(
    X_all: pd.DataFrame, X_panel: pd.DataFrame, n_fold: int, stratify
) -> pd.Series:
    """Mutation signature false negative rate from a spectrum of mutations after `n_fold` coarse graining."""
    if n_fold == 1:  # No coarse graining.
        W, W_panel = deconvolute_spectra(X_all, X_panel)
        return signature_fnr(W, W_panel)

    X_nfold, X_panel_nfold = pair_wise_coarse_grain(
        X_all, X_panel, n_fold=n_fold, stratify=stratify
    )
    W_nfold, W_panel_nfold = deconvolute_spectra(X_nfold, X_panel_nfold)
    return signature_fnr(W_nfold, W_panel_nfold)


def deconvolute_spectra(
    *X_spectra: pd.DataFrame, drop_artefacts: bool = True
) -> list[pd.DataFrame]:
    decomposer = CosmicNMF(cosmic_signature="single_base_substitutions")
    X_signatures = []
    for X_spectrum in X_spectra:
        w = decomposer.transform(X_spectrum)
        if drop_artefacts:
            w = w.drop(columns=POSSIBLE_SBS_SEQUENCING_ARTEFACTS)
        X_signatures.append(w)
    return X_signatures


def extrapolate_fnr(
    X_all: pd.DataFrame,
    X_panel: pd.DataFrame,
    n_max: int,
    stratify,
    n_iterations: int = 100,
    random_state: int | None = 42,
) -> dict[int, Estimate]:
    """Extrapolate panel FNR of signatures to larger size by coarse graining patients.

    The extrapolated panel size is in integral multiples: (1,...,`n_max`).

    Args:
        X_all: Complete whole genome (non-synonymous) mutation spectrum.
        X_panel: Mutation spectrum of the panel of interest.
        n_max: Range of patients (1,...,`n_max`) to coarse grain over.
        stratify: Only coarse grain over patients within strata.
        n_iterations: Compute confidence interval by bootstrapping this many times.

    Returns: The keys of the dictionary is `n` (extrapolated panel size: n * length)
        and the value the bootstrapped estimates of the FNR.
    """
    kwargs = dict(n_iterations=n_iterations, random_state=random_state)
    estimates = {}
    for i in range(1, n_max + 1):
        fnr_i = lambda a, b: signature_fnr_from_spectrum(
            a, b, n_fold=i, stratify=stratify
        )
        estimate_i = bootstrap_score(X_all, X_panel, metric=fnr_i, **kwargs)
        estimates[i] = estimate_i
        print(".", end="")

    print("")
    return estimates


def _fnr(y_true, y_pred):
    """False negative rate."""
    is_fn = (y_pred == 0) & (y_true == 1)
    fn = is_fn.sum().astype(float)
    p = (y_true == 1).sum().astype(float)
    return np.divide(fn, p, out=np.zeros_like(fn), where=p != 0)


def signature_fnr(W_true: pd.DataFrame, W_pred: pd.DataFrame) -> pd.Series:
    """False negative rate for each signature in `X_pred` with respect to `X_true"""
    x_cols = W_true.columns
    false_negative_rates = pd.Series(0.0, index=x_cols)
    for c in x_cols:
        x_true = (W_true[c] > 0).astype(int)
        x_pred = (W_pred[c] > 0).astype(int)
        false_negative_rates[c] = _fnr(x_true, x_pred)
    return false_negative_rates
