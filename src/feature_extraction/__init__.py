from pathlib import Path
from typing import Literal

from pandas import read_csv

RESOURCE_DIR = Path(__file__).parent.resolve() / "resources"
COSMIC_MUTATIONAL_SIGNATURES = {
    "single_base_substitutions": RESOURCE_DIR / "COSMIC_v3.3_SBS_GRCh37.txt",
    "doublet_base_substitutions": RESOURCE_DIR / "COSMIC_v3.3_DBS_GRCh37.txt",
    "indel": RESOURCE_DIR / "COSMIC_v3.3_ID_GRCh37.txt",
    "cnv": RESOURCE_DIR / "COSMIC_v3.3_CN_GRCh37.txt",
}


def get_feature_names(
    representation: Literal[
        "single_base_substitutions",
        "doublet_base_substitutions",
        "indel",
        "cnv",
    ],
    signatures: bool = True,
) -> list:
    """Fetch feature/signature names of a given spectrum.

    Args:
        signatures: If True, return COSMIC signature names, otherwise the
            transition names.
    """
    # Extract from mutational signature file.
    dataframe = read_csv(
        COSMIC_MUTATIONAL_SIGNATURES[representation], sep="\t", index_col=0
    )
    if signatures:
        return dataframe.columns.to_list()
    return dataframe.index.to_list()
