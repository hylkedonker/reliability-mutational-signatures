from pathlib import Path
from typing import Literal, Optional

from pandas import DataFrame, read_csv
from sklearn.decomposition import NMF

from src.feature_extraction import COSMIC_MUTATIONAL_SIGNATURES


class CosmicNMF(NMF):
    """Load NMF model fit with COSMIC GRCh37 signatures.

    Given the mutational signature H, the `transform` method computes `W` so that
    X = WH
    where X is the mutation spectrum.
    """

    def __init__(
        self,
        cosmic_signature: Literal[
            "single_base_substitutions", "doublet_base_substitutions", "indel", "cnv"
        ],
        tol: float = 1e-6,
        max_iter: int = 10_000,
    ):
        """
        Load NMF COSMIC GRCh37 mutational signature checkpoint.

        Args:
            cosmic_signature: Input features correspond to 96 single base
                substution (SBS), 78 double base substitutions (DBS), 83
                indels mutational spectrum, or 48 copy number variant classes.
        """
        self.cosmic_signature = cosmic_signature
        H = read_csv(
            COSMIC_MUTATIONAL_SIGNATURES[cosmic_signature], sep=r"\t", index_col=0
        ).T
        self.n_components_, self.n_features_in_ = H.shape

        super().__init__(
            n_components=self.n_components_,
            max_iter=max_iter,
            solver="cd",
            init="nndsvda",
            tol=tol,
        )
        self.components_ = H.to_numpy()
        self.feature_names_in_ = H.columns
        self.feature_names_out_ = H.index

    def transform(self, X):
        """Transform back to pandas dataframe."""
        X_numpy = super().transform(X)
        return DataFrame(X_numpy, index=X.index, columns=self.feature_names_out_)


def transform_cosmic_signatures(
    input_folder: Path, target_folder: Optional[Path] = None
) -> dict:
    """
    Transform mutation spectra to COSMIC signature activations.

    Compute COSMIC signature activations corresponding to the mutation spectra:
    - 96 single base substitutions (SBS),
    - 78 double base substitutions (DBS),
    - 83 indels.

    Args:
        input_folder: Location of SBS, DBS and indel tab-separated value (TSV)
            spectrum file. Samples along the columns and variants on the rows.
        target_folder: Store signature activations in TSV format. Generates the
            files activations-{SBS96,DBS78,ID83}.tsv with samples along the rows
            and signature activations on the columns.

    Returns: For each spectrum a dataframe
    """

    # For each mutational spectrum, compute
    #  W' = X' H^{-1}
    # using COSMIC signature matrix H.
    X_sbs96 = read_csv(
        input_folder / "mutation-class.SBS96.tsv", sep="\t", index_col=0
    ).T
    X_db78 = read_csv(
        input_folder / "mutation-class.DBS78.tsv", sep="\t", index_col=0
    ).T
    X_id83 = read_csv(input_folder / "mutation-class.ID83.tsv", sep="\t", index_col=0).T

    decomposer_sbs96 = CosmicNMF(cosmic_signature="single_base_substitutions")
    decomposer_db78 = CosmicNMF(cosmic_signature="doublet_base_substitutions")
    decomposer_id83 = CosmicNMF(cosmic_signature="indels")

    W_sbs96 = decomposer_sbs96.transform(X_sbs96)
    W_db78 = decomposer_db78.transform(X_db78)
    W_id83 = decomposer_id83.transform(X_id83)

    if target_folder is not None:
        target_folder.mkdir(exist_ok=True, parents=True)
        W_sbs96.to_csv(target_folder / "activations-SBS96.tsv", sep="\t")
        W_db78.to_csv(target_folder / "activations-DB78.tsv", sep="\t")
        W_id83.to_csv(target_folder / "activations-ID83.tsv", sep="\t")

    return {
        "single_base_substitutions": W_sbs96,
        "doublet_base_substitutions": W_db78,
        "indel": W_id83,
    }
