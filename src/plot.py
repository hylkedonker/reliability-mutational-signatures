from matplotlib import pyplot as plt
from numpy.typing import NDArray
from numpy import maximum
from pandas import DataFrame, concat
from statkit.views import format_p_value


def plot_pvalue(x_values, y_values, pvalue: float, ax):
    """Annotate groups with p-value text."""
    x1, x2 = x_values[0], x_values[1]
    height = 0.05 * y_values.max()
    y = y_values.max() + height

    # Plot bar.
    ax.plot([x1, x1, x2, x2], [y, y + height, y + height, y], lw=1.5, c="k")

    # Plot p-value text.
    p_label = format_p_value(pvalue, latex=True, symbol="p", format="compact")
    ax.text((x1 + x2) * 0.5, y + height, p_label, ha="center", va="bottom", color="k")

    # Adjust limits to encompass annotation.
    ax.set_ylim(ax.get_ylim()[0], y + 4 * height)


def plot_image(
    filename: str,
    x: float,
    y: float,
    width: float | None = None,
    height: float | None = None,
    ax=None,
):
    """Render image as axes at given location.

    Args:
        filename: Path to image to plot.
        x, y: Position of the centre of the image in data coordinates.
        width, height: Size of image. One of the two arguments can be left unspecified
            or set to "auto" to infer its value from the image's aspect ratio.
        ax: Attach image to this axes (use current axes when unspecified).

    Returns:
        New axes with the image.
    """
    image = plt.imread(filename)

    # Infer width/height by aspect ratio.
    aspect_ratio = image.shape[0] / image.shape[1]

    assert not (
        width is None and height is None
    ), "Either width, height or both have to be specified."

    if height is None or height == "auto":
        height = width / aspect_ratio
    elif width is None or width == "auto":
        width = height * aspect_ratio

    # Use current axes if not specified.
    if ax is None:
        ax = plt.gca()

    fig = ax.get_figure()

    # Compute axes coordinates by transforming:
    # Data coordinates --> figure coordinates --> axes coordinates.
    img_location = [(x - width / 2, y - height / 2), (x + width / 2, y + height / 2)]

    # Transform data coordinates --> figure coordinates.
    fig_coords = ax.transData.transform(img_location)
    # Transform figure coordinates --> axes coordinates.
    ax_coords = fig.transFigure.inverted().transform(fig_coords)

    # Positions: left, bottom, width, height:
    ax_width = ax_coords[1, 0] - ax_coords[0, 0]
    ax_height = ax_coords[1, 1] - ax_coords[0, 1]
    xy_size = [ax_coords[0, 0], ax_coords[0, 1], ax_width, ax_height]

    img_ax = fig.add_axes(xy_size, anchor="C", zorder=1)
    img_ax.imshow(image)
    img_ax.axis("off")

    return img_ax


def get_y_and_yerr(
    table: DataFrame, quantile_range: tuple[float, float]
) -> tuple[float, NDArray]:
    """Compute mean and matplotlib error bars based on (90 %) quantile range.

    Returns:
        The mean and the matplotlib error bars.
    """
    lower, upper = quantile_range
    lower_quartile = table.quantile(lower)
    upper_quartile = table.quantile(upper)
    mean = table.mean()

    errors = concat(
        [mean - lower_quartile, maximum(upper_quartile - mean, 0)], axis=1
    ).T
    return mean, errors.to_numpy()


def truncate_signatures(X_signatures, signatures: int | list):
    """Truncate matrix to `n_top` mean signatures."""
    if isinstance(signatures, int):
        top_signatures = (
            X_signatures.mean(axis=0).sort_values(ascending=False).index[:signatures]
        )
    else:
        top_signatures = signatures

    rest_signatures = X_signatures.columns[~X_signatures.columns.isin(top_signatures)]
    truncated_signatures = X_signatures[top_signatures].copy()
    truncated_signatures["rest"] = X_signatures[rest_signatures].sum(axis=1)
    return truncated_signatures
