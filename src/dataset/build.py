from pathlib import Path
from typing import Callable

from pandas import DataFrame

from src.dataset import ARTIFACT_DIR


def build_filtered_vcfs(
    dataframe,
    artifact_dir: Path,
    filter_function: Callable[[Path, Path], None],
    filename_column: str = "vcf",
    verbose: bool = False,
) -> DataFrame:
    """Apply synonymous filter and generate new dataframe.

    Usage:
        build_filtered_vcfs(
            df,
            Path("/path/to/my/artifacts/"),
            filter_function=filter_synonymous_variants,
            verbose=True,
        )
    """
    target_dir = artifact_dir / "non-synonymous"
    target_dir.mkdir(exist_ok=True, parents=True)

    new_dataframe = dataframe.copy()
    for idx, row in dataframe.iterrows():
        source_vcf = row[filename_column]
        target_vcf = target_dir / source_vcf.name.replace(".gz", "").replace(
            ".purple", ""
        )
        # Don't store the full path in the dataframe.
        prefix = str(ARTIFACT_DIR) + "/"
        new_dataframe.loc[idx, filename_column] = str(target_vcf).removeprefix(prefix)
        new_dataframe.loc[idx, filename_column + "_unfiltered"] = str(
            source_vcf
        ).removeprefix(prefix)

        if verbose:
            print("Filtering", source_vcf.name, "..")
        else:
            print(".", end="")

        filter_function(input_vcf=source_vcf, filtered_vcf=target_vcf)
    print()
    return new_dataframe
