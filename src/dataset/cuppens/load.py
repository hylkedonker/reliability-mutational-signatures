from pathlib import Path
import re

from pandas import DataFrame, read_csv

from src.dataset import ARTIFACT_DIR, CUPPENS_DIR


def get_sample_metric(path: Path, field_name) -> float:
    """Extract coverage region (in megabases) from TSO500 metrics CSV file."""
    df = read_csv(path, index_col=2, header=None)
    x = df.loc[field_name]
    # Last column contains the value.
    return x.iloc[-1]


def _get_sample_name(path: Path) -> str:
    sample_id = path.name.split(".")[0]
    pattern = r"[-_]?UP\d\d[-_]?"
    mo = re.search(pattern, sample_id)
    token = mo.group(0)
    return sample_id.removeprefix(token).removesuffix(token)


def load() -> DataFrame:
    """Load Cuppens test dataset of TSO500 variants."""
    dataframe_tsv = Path(ARTIFACT_DIR / "cuppens_dataset.tsv")

    # Build artifacts if necessary.
    if dataframe_tsv.exists():
        return read_csv(dataframe_tsv, index_col=0, sep="\t")

    # Extract coverage size from files.
    metric_files = tuple((CUPPENS_DIR / "samples").glob("*.csv"))
    region_megabases = map(
        lambda x: get_sample_metric(x, "Eligible Region (MB)"), metric_files
    )
    tmb = map(lambda x: get_sample_metric(x, "TMB"), metric_files)
    tmb_non_synonymous = map(lambda x: get_sample_metric(x, "Nonsyn TMB"), metric_files)

    sample_names = [_get_sample_name(p) for p in metric_files]
    tsv_files = [
        CUPPENS_DIR / "samples" / (path.name.split(".")[0] + "_TMB_Trace.tsv")
        for path in metric_files
    ]

    # Clinical outcomes.
    treatment_outcomes = read_csv(CUPPENS_DIR / "samples.csv", index_col=0).loc[
        sample_names
    ]
    durable_benefit = treatment_outcomes.pop(
        "Observed Durable Clinical Response = SD for at least > 6 months? YES=1; NO=0"
    )
    best_response = treatment_outcomes.pop("Best Response")
    exposure = treatment_outcomes.pop("Exposure to therapy (months)")

    X = DataFrame(
        {
            "tsv": tsv_files,
            "coverage size (Mb)": tuple(region_megabases),
            "tmb all (Mb^-1)": tuple(tmb),
            "tmb non-synonymous (Mb^-1)": tuple(tmb_non_synonymous),
            "durable_benefit": durable_benefit,
            "best_response": best_response,
            "exposure_to_therapy (months)": exposure,
        },
    )
    X.index = X.index.map(lambda s: f"CUPP_{s}")

    X.to_csv(dataframe_tsv, sep="\t")

    return X
