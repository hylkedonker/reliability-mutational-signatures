from pathlib import Path
import os


DATASET_DIR = Path(os.path.dirname(__file__))
HARTWIG_DIR = DATASET_DIR / "hartwig"
CUPPENS_DIR = DATASET_DIR / "cuppens"
CUPPENSDRAGEN_DIR = DATASET_DIR / "dragen"
ARTIFACT_DIR = DATASET_DIR / ".." / ".." / "artifacts"
