from datetime import date, timedelta
from pathlib import Path

from numpy import nan
from pandas import DataFrame, NaT, Series, concat, read_csv, to_datetime, to_timedelta

from src.dataset import ARTIFACT_DIR, HARTWIG_DIR
from src.dataset.build import build_filtered_vcfs
from src.dataset.utils import durable_benefit
from src.feature_extraction.step1.filter_vcf import filter_synonymous_variants


def _get_hartwig_dna_sample_names() -> list:
    """Samples for which we have measured variants."""
    names = []
    for vcf_file in (HARTWIG_DIR / "vcf").glob("*.vcf.gz"):
        names.append(vcf_file.name.removesuffix(".purple.somatic.vcf.gz"))
    return names


def _get_hartwig_rna_sample_names() -> list:
    """Samples for which RNA was measured."""
    X_counts = (
        read_csv(ARTIFACT_DIR / "htseq-counts.tsv", index_col=0, sep="\t")
        .drop(columns="gene")
        .sort_index()
        .transpose()
    )
    # Add HRTW_ prefix.
    X_counts.index = X_counts.index.map(lambda x: "HRTW_" + x.split("_")[0])
    return X_counts.index.tolist()


def _compute_age(X: DataFrame) -> Series:
    baseline = to_datetime(X["treatmentStartDate"])
    birth = (
        X["birthYear"].apply(lambda x: date(year=x, month=1, day=1)).apply(to_datetime)
    )
    age = round((baseline - birth) / timedelta(weeks=52))
    return age.astype(int)


def _overall_survival(X_clinic: DataFrame) -> DataFrame:
    """Extract overall survival data by determining last follow up, if necessary."""
    survival = DataFrame(
        0,
        columns=["os_days", "os_event"],
        index=X_clinic.index,
    )

    t0 = to_datetime(X_clinic["treatmentStartDate"])

    t_stop = to_datetime(X_clinic["treatmentEndDate"])
    t_death = to_datetime(X_clinic["deathDate"])
    survival["os_event"] = t_death.notna().astype(int)
    survival["os_days"] = t_death - t0

    # There was one case where the death date preceded treatment. Censor at last follow
    # up.
    is_mistake = t_death < t0
    survival.loc[is_mistake, "os_event"] = 0
    is_unknown = t_death.isna() | is_mistake

    # Use last available follow up (treatment end date, or response datum).
    survival.loc[is_unknown, "os_days"] = t_stop.loc[is_unknown] - t0.loc[is_unknown]
    days = concat(
        [survival["os_days"], X_clinic["pfs_days"].fillna(NaT)], axis="columns"
    )
    survival.loc[is_unknown, "os_days"] = days.loc[is_unknown].max(axis=1)

    # Print key study dates.
    print("Start of accruel:", t0.min())
    print("End of accruel:", t0.max())
    print("End of follow-up:", max(t_stop.max(), t_death.max()))

    return survival


def _progression_free_survival(samples: DataFrame) -> DataFrame:
    """Compute progression free survival."""
    response_map = {
        "SD": 0,
        "CR": 0,
        "PR": 0,
        "PD": 1,
        "Clinical progression": 1,
        "ND": nan,
    }
    progression = samples["response"].map(response_map)

    survival = DataFrame(
        0,
        columns=["pfs_days", "pfs_event"],
        index=samples.sampleId.unique(),
    )
    is_progression = samples[progression == 1]
    survival.loc[is_progression["sampleId"], "pfs_event"] = 1

    # Compute amount of follow-up per response.
    samples["responseDate"] = to_datetime(samples["responseDate"])
    t0 = to_datetime(samples["startDate"])

    samples["day_diff"] = samples["responseDate"] - t0
    # Most recent follow-up is time-to-progression/censor.
    follow_up = samples.groupby(["sampleId"])["day_diff"].max()

    survival.loc[follow_up.index, "pfs_days"] = follow_up
    # Add dataset prefix.
    survival.index = survival.index.map(lambda x: f"HRTW_{x}")
    return survival


def load_hartwig_metadata() -> DataFrame:
    """Compile patient characteristics of dataset."""

    def _n_patients(data_frame):
        return data_frame["#patientId"].unique().shape[0]

    hartwig = read_csv(HARTWIG_DIR / "metadata" / "metadata.tsv", sep="\t")

    n_total_patients = _n_patients(hartwig)
    print("Number of patients in DR #094", n_total_patients)

    print("Excluding:")
    # 1) Exclude patient without DNA samples.
    dna_samples = _get_hartwig_dna_sample_names()

    has_dna = hartwig.sampleId.isin(dna_samples)
    n_no_dna = _n_patients(hartwig[~has_dna])
    print("- No DNA (n={})".format(n_no_dna))
    hartwig = hartwig[has_dna]

    assert _n_patients(hartwig) + n_no_dna == n_total_patients

    # 2) Exclude patients with incorrect therapy.
    wrong_therapy = hartwig.treatmentType.isin(
        ["Targeted therapy", "Chemotherapy"]
    ) | hartwig.treatment.str.contains("osimertinib", case=False)

    # N.B., the metadata file contains multiple samples per patient. So there is patient
    # overlap between wrong samples and correct samples.
    hartwig = hartwig[~wrong_therapy]
    n_patients_immuno = _n_patients(hartwig)
    n_wrong_therapy = n_total_patients - n_no_dna - n_patients_immuno
    print("- Wrong therapy (n={})".format(n_wrong_therapy))

    assert _n_patients(hartwig) + n_wrong_therapy + n_no_dna == n_total_patients

    # 3) Exclude patients with incorrect lung cancer subtype.
    is_nsclc = hartwig.cancerSubtype.str.lower() == "non-small cell"
    n_incorrect_subtype = _n_patients(hartwig[~is_nsclc])
    print("- Incorrect lung cancer substype (n={})".format(n_incorrect_subtype))
    hartwig = hartwig[is_nsclc]

    assert (
        _n_patients(hartwig) + n_incorrect_subtype + n_wrong_therapy + n_no_dna
        == n_total_patients
    )

    # 4) Exclude patients without outcome.
    sample_outcomes = read_csv(
        HARTWIG_DIR / "metadata" / "responses_by_sample.tsv", sep="\t"
    )

    # Exclude samples of excluded patients.
    sample_outcomes = sample_outcomes[sample_outcomes.sampleId.isin(hartwig.sampleId)]

    # For the following patients, we have no outcome of corresponding sample.
    patients_no_outcome = set(hartwig["#patientId"]) - set(
        sample_outcomes["#patientId"]
    )
    n_no_outcome = len(patients_no_outcome)

    # 5)
    # For the following patients we don't have RECIST1.1 measurements,
    # but they almost immediately stopped treatment and then died. So, we
    # call them progressors.
    included = {
        # key: #patientId; value: intended index.
        "CPCT02020452": "HRTW_CPCT02020452T",
        "CPCT02040092": "HRTW_CPCT02040092T",
        "CPCT02010333": "HRTW_CPCT02010333TII",
        "CPCT02010536": "HRTW_CPCT02010536T",
        "CPCT02120140": "HRTW_CPCT02120140T",
        "CPCT02011086": "HRTW_CPCT02011086T",
        "CPCT02011069": "HRTW_CPCT02011069T",
        "CPCT02020964": "HRTW_CPCT02020964T",
    }

    excl_patients_follow_up = patients_no_outcome - set(included)

    n_excl_follow_up = len(excl_patients_follow_up)
    print(
        f"- Unresolvable follow up (m={n_excl_follow_up}). "
        f"(Patients without follow-up: m = {n_no_outcome}; "
        f"Patients re-identified: m={n_no_outcome - n_excl_follow_up})"
    )
    hartwig = hartwig[~hartwig["#patientId"].isin(excl_patients_follow_up)]
    sample_outcomes = sample_outcomes[sample_outcomes.sampleId.isin(hartwig.sampleId)]

    assert (
        _n_patients(hartwig)
        + n_incorrect_subtype
        + n_excl_follow_up
        + n_wrong_therapy
        + n_no_dna
        == n_total_patients
    )

    # Verify that the number of patients and samples match.
    assert len(sample_outcomes["sampleId"].unique()) + len(included) == len(
        hartwig["sampleId"].unique()
    )
    assert len(sample_outcomes["#patientId"].unique()) + len(included) == len(
        hartwig["#patientId"].unique()
    )

    # Compute outcomes.
    pfs = _progression_free_survival(sample_outcomes)
    db = durable_benefit(pfs)

    hartwig.set_index("sampleId", inplace=True)
    hartwig.index = hartwig.index.map(lambda x: f"HRTW_{x}")

    # Verify that outcome indices are a subset of hartwig index.
    assert set(pfs.index) - set(hartwig.index) == set()
    assert set(db.index) - set(hartwig.index) == set()

    # Add calculated response data.
    hartwig.loc[pfs.index, ["pfs_days", "pfs_event"]] = pfs
    os = _overall_survival(hartwig)
    hartwig.loc[os.index, ["os_days", "os_event"]] = os
    hartwig.loc[db.index, "durable_benefit"] = db

    # We don't have radiographic response for `missing_response` samples.
    # However, these patients died quickly after treatment initiation, and
    # died shortly after. We therefore set progression time to treatment end
    # date.
    t0 = to_datetime(hartwig["treatmentStartDate"])
    t1 = to_datetime(hartwig["treatmentEndDate"])

    index_included = list(included.values())
    hartwig.loc[index_included, "pfs_days"] = (
        t1.loc[index_included] - t0.loc[index_included]
    )
    hartwig["pfs_days"] = hartwig["pfs_days"].fillna(NaT)
    hartwig.loc[index_included, "pfs_event"] = 1
    hartwig.loc[index_included, "durable_benefit"] = 0

    has_db = ~hartwig["durable_benefit"].isna()
    # 6)
    print(
        "Total: n =",
        _n_patients(hartwig),
        "(with durable benefit label n = {})".format(_n_patients(hartwig[has_db])),
    )

    # 7) Patients with RNA
    rna_samples = _get_hartwig_rna_sample_names()
    hartwig_rna = hartwig[hartwig.index.isin(rna_samples)]
    n_rna_patients = len(hartwig_rna)
    print(f"Patients with RNA: m = {n_rna_patients}")
    # Add field indicating presence of RNA.
    hartwig["rna"] = 0
    hartwig.loc[hartwig_rna.index, "rna"] = 1

    # 8) Exclude RNA samples with bad quality.
    rna_sample_blacklist = [
        "HRTW_CPCT02010807T",
        "HRTW_CPCT02010455T",
        "HRTW_CPCT02040115T",
        "HRTW_CPCT02010567T",  # Wrong therapy.
    ]
    sample_ok = ~hartwig_rna.index.isin(rna_sample_blacklist)

    # Add field to indicating that RNA passed quality assurance.
    hartwig["rna_quality_pass"] = nan
    hartwig.loc[hartwig_rna.index, "rna_quality_pass"] = 0
    hartwig_rna = hartwig_rna[sample_ok]
    hartwig.loc[hartwig_rna.index, "rna_quality_pass"] = 1

    print("Patient with good quality RNA: n = ", len(hartwig_rna))

    print(
        "Patients with RNA and outcome label: n = ",
        len(hartwig_rna[hartwig_rna["durable_benefit"].notna()]),
    )
    hartwig["rna_quality_assurance_passed"] = 0
    hartwig.loc[hartwig_rna.index, "rna_quality_assurance_passed"]

    # Additional polishing of the data.
    hartwig["age"] = _compute_age(hartwig)
    hartwig["tumor_purity"] = hartwig.pop("tumorPurity")

    # For definition of platinum doublets and triplets:
    # Novel chemotherapy regimens for advanced lung cancer: have we reached a plateau?
    hartwig["treatment"] = (
        hartwig["treatment"]
        .str.lower()
        .replace(
            {
                "atezolizumab/bevacizumab/carboplatin/paclitaxel": "atezolizumab with chemotherapy",
                "atezolizumab/cisplatin/pemetrexed": "atezolizumab with chemotherapy",
                "bevacizumab/nivolumab": "bevacizumab + nivolumab",
                "carboplatin/pembrolizumab/pemetrexed": "Pembrolizumab with chemotherapy",
                "carboplatin/paclitaxel/pembrolizumab": "Pembrolizumab with chemotherapy",
                "carboplatin/ipilimumab/nivolumab/pemetrexed": "Ipilimumab + Nivolumab with chemotherapy",
                "cisplatin/nivolumab/pemetrexed": "nivolumab with chemotherapy",
                "ipilimumab/nivolumab": "ipilimumab + nivolumab",
                "mk 5890/pembrolizumab": "mk 5890 + pembrolizumab",
                "carboplatin/cisplatin/pembrolizumab/pemetrexed": "pembrolizumab with chemotherapy",
            }
        )
        .str.lower()
        .replace(
            {
                "mk 5890": "αTNFRSF7",
                "atezolizumab": "αPD-L1",
                "pembrolizumab": "αPD-1",
                "nivolumab": "αPD-1",
                "durvalumab": "αPD-L1",
                "ipilimumab": "αCTLA-4",
                "bevacizumab": "αVEGF-A",
            },
            regex=True,
        )
    )
    has_chemo = (
        hartwig.pop("hasSystemicPreTreatment").replace({"Yes": 1, "No": 0}).astype(int)
    )
    has_radio = (
        hartwig.pop("hasRadiotherapyPreTreatment")
        .replace({"Yes": 1, "No": 0})
        .astype(int)
    )
    hartwig["prior_therapy"] = ((has_chemo.to_numpy() << 1) | has_radio).replace(
        {
            0: "naive",
            1: "radiotherapy",
            2: "chemotherapy",
            3: "radiotherapy + chemotherapy",
        }
    )
    hartwig["pre_treatment"] = has_chemo | has_radio

    # All these patients are metastatic, so stage IV.
    hartwig["stage"] = "IV"
    hartwig["histology"] = nan
    # As per documentation: https://github.com/hartwigmedical/documentation/
    hartwig["tissue"] = "fresh frozen"

    # Based on the estimate of the paper:
    # The GENCODE exome: sequencing the complete human exome.
    hartwig["region covered (mb)"] = 47.9

    hartwig["smoker"] = nan

    hartwig["pfs_days"] = hartwig["pfs_days"].dt.days
    hartwig["os_days"] = hartwig["os_days"].dt.days

    return hartwig[
        [
            "age",
            "gender",
            "histology",
            "prior_therapy",
            "pre_treatment",
            "smoker",
            "tumor_purity",
            "treatment",
            "stage",
            "tissue",
            "durable_benefit",
            "pfs_days",
            "pfs_event",
            "os_days",
            "os_event",
            "region covered (mb)",
            "rna",
            "rna_quality_pass",
        ]
    ]


def load() -> DataFrame:
    """Load Hartwig dataset of non-synonymous filtered variants."""
    dataframe_tsv = Path(ARTIFACT_DIR / "hartwig_dataset.tsv")
    IN_SILICO_DIR = Path(ARTIFACT_DIR / "hartwig" / "vcf_in_silico")

    # Build artifacts if necessary.
    if not dataframe_tsv.exists():
        dataframe = load_hartwig_metadata()
        vcf_name = dataframe.index.map(
            lambda x: x.removeprefix("HRTW_") + ".purple.somatic.vcf.gz"
        )
        cnv_name = dataframe.index.map(
            lambda x: x.removeprefix("HRTW_") + ".purple.cnv.somatic.tsv"
        )
        dataframe["vcf"] = HARTWIG_DIR / "vcf" / vcf_name
        dataframe["cnv"] = HARTWIG_DIR / "cnv" / cnv_name
        filtered_vcf_name = vcf_name.map(lambda x: "filtered_" + x)

        # Check that all files exist.
        assert dataframe["cnv"].map(lambda x: x.exists()).all()
        assert dataframe["vcf"].map(lambda x: x.exists()).all()

        X = build_filtered_vcfs(
            dataframe,
            ARTIFACT_DIR / "hartwig",
            filter_function=filter_synonymous_variants,
            filename_column="vcf",
            verbose=True,
        )

        panels = [
            "tso500",
            "foundation_one_cdx",
            "msk_341",
            "msk_410",
            "msk_468",
            "pan_cancer",
        ]
        for panel_name in panels:
            column_name = "vcf_in_silico_" + panel_name
            X[column_name] = (
                IN_SILICO_DIR / panel_name / "unfiltered" / filtered_vcf_name
            )
            # Check that all files exist.
            assert X[column_name].map(lambda x: x.exists()).all()

            # Filter synonymous variants.
            X = build_filtered_vcfs(
                X,
                IN_SILICO_DIR / panel_name,
                filter_function=filter_synonymous_variants,
                filename_column=column_name,
                verbose=True,
            )
            X[column_name] = X[column_name].map(
                lambda x: str(x).removeprefix(str(ARTIFACT_DIR) + "/")
            )

        X["cnv"] = X["cnv"].map(lambda x: str(x).removeprefix(str(ARTIFACT_DIR) + "/"))
        X.to_csv(dataframe_tsv, sep="\t")
    else:
        X = read_csv(dataframe_tsv, index_col=0, sep="\t")
        X["pfs_days"] = to_timedelta(X["pfs_days"], unit="days")

    return X
